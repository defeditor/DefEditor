(asdf:operate 'asdf:load-op :defdoc.contrib.project-website)

(defpackage :defeditor.website
    (:use :defdoc :defdoc.elements :defdoc.layout :common-lisp :defdoc.frontends.basic
          :defdoc.contrib.project-website))

(in-package :defeditor.website)

(defabbreviation DefDoc (doc italic () "Def") (doc small-caps () "Doc"))
(defabbreviation DefEditor (doc italic () "Def") (doc small-caps () "Editor"))

(defun gen-src-link (target)
  (concatenate 'string "/cgi-bin/viewcvs.cgi/DefEditor/src" target "?cvsroot=defeditor"
               (unless (eql (aref target (1- (length target))) #\/)
                 "&rev=HEAD&content-type=text/vnd.viewcvs-markup")))

(defun gen-mailing-list-link (target)
  (concatenate 'string "/mailman/listinfo/defeditor-" target))

(defdoc index (project-website name (doc DefEditor)
                               short-description "An extensible, dynamic strcutured document editor"
                               author "Rahul Jain")
  (documentation ()
                 (paragraph ()
                            "None right now."))
  (code ()
        (paragraph ()
                   "The code is in a rather infantile state, but what there is is "
                   (link (url (gen-src-link "/")) "publically available")". "
                   (DefEditor)" will use "(link (url (gen-src-link "/presentation.lisp")) "CLIM for the UI")". "
                   "Document structure will be specified "
                   (link (url (gen-src-link "/structure.lisp")) "abstractly")", "
                   "with a specialized "(link (url (gen-src-link "/lisp-structure-reader.lisp")) "lisp reader")" "
                   "reading in code into specialized "(link (url (gen-src-link "/lisp-structure.lisp")) "lisp structural objects")".")
        (section (title "Plans")
                 (paragraph ()
                            "All editing will be done on valid objects, and "
                            "syntactic information as well as documentation will be tracked by the editor, "
                            "as implemented by the module for that syntax. "
                            "Syntactic information will be used to "
                            "restrict the completion options to those that make sense, "
                            "to optimize line-wrapping and indentation for readability, and "
                            "to highlight elements according to their syntactic role. "
                            "The wrapping engine from "(DefDoc)" is planned to be re-used for line-wrapping in "(DefEditor)". "
                            "Things like floating comments will also be supported by using special tags "
                            "in the normal comment syntax for the language of the document. "
                            "There will be a pretty-printer for outputting code as a "(DefDoc)" document "
                            "for printing or other output. "
                            "In the same spirit, organization of code can be indicated using "
                            "hierarchial sectioning, persisted as special tagged comments.")))
  (communication ()
                 (paragraph ()
                            "There are 3 mailing lists for "(DefEditor)", "
                            (link (url (gen-mailing-list-link "announce")) "defeditor-announce")", "
                            (link (url (gen-mailing-list-link "devel")) "defeditor-devel")", and "
                            (link (url (gen-mailing-list-link "cvs")) "defeditor-cvs")".")))
