(defclass structure-element ()
  ())

(defclass structure-branch (structure-element))

(defclass structure-branch-list (structure-branch)
  ((structure-contents :initarg structure-contents
                       :accessor strcuture-contents
                       :initform '()
                       :type list)))

(defclass structure-branch-item (structure-branch)
  ((structure-content :initarg structure-content
                      :accessor structure-content
                      :initform (error "Structure's content must be defined."))))

(defmethod structure-contents ((structure structure-branch-item))
  (list (structure-content structure)))

(defmethod (setf structure-contents) (new-value (structure structure-branch-item))
  (if (and (consp new-value) (endp (rest new-value)))
      (setf (structure-content structure) (first new-value))
      (error "Can't set single-item branch's contents to multiple items.")))

(defclass structure-leaf (structure-element)
  ())
