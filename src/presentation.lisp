(in-package :defeditor)

(define-presentation-type structure-element ())

(defclass structured-document-pane (vbox-pane)
  (())
  (:default-initargs :scroll-bars :vertical :default-view +graphical-view+))

(define-presentation-method present (object (type structure-element) stream (view graphical-view) &key)
  (declare (ignore view))
  (make-pane 'structure-element-pane structure-element))

(defclass structure-element-pane (multiple-child-composite-pane)
  ((begin-pane :reader begin-pane)
   (end-pane :reader end-pane)))
