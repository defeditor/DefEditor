;;;; ***
;;;; ABSTRACT SYNTAX ELEMENTS
;;;; ***

(defclass macro-character-mixin ()
  ((macro-character :allocation :class
                    :reader macro-character)))

(defclass dispatching-macro-character-mixin (macro-character-mixin)
  ((dispatching-macro-character :allocation :class
                                :reader dispatching-macro-character)
   (dispatching-macro-argument :initarg dispatching-macro-argument
                               :initform nil
                               :type (or null (integer 0)))))

(defclass terminated-macro (macro-character-mixin structure-branch-list)
  (macro-terminating-character :allocation :class
                               :reader macro-terminating-character))

;;;; ***
;;;; CONCRETE SYNTAX ELEMENTS
;;;; ***

;;;
;;; Basic Leaves
;;;

(defclass symbol-element (structure-leaf)
  ((symbol-element-package :initarg symbol-element-package
                           :reader symbol-element-package
                           :initform nil
                           :type (or null string))
   (symbol-element-name :initarg symbol-element-name
                        :reader symbol-element-name
                        :initform (make-string)
                        :type string)
   (symbol-internal-p :initarg symbol-internal-p
                      :reader symbol-internal-p
                      :initform nil
                      :type boolean)))

(defclass symbol-macro-element (symbol-element)
  ())

;;;
;;; Basic Branches (parenthesized forms)
;;;

(defclass unknown-branch (structure-branch)
  ())

(defclass application (structure-branch)
  ((application-operator :initarg application-operator
                         :accessor application-operator
                         :initform (error "Application's operator must be defined."))))

(defclass lambda-list (structure-branch)
  ())

(defclass binding-list (structure-branch)
  ())

;;;
;;; Macro Characters
;;;

(defclass double-quote (terminated-macro)
  ((macro-character :initform #\")
   (macro-terminating-character :initform #\")))

(defclass single-quote (macro-character-mixin structure-branch-item)
  ((macro-character :initform #\')))

(defclass comma (macro-character-mixin structure-branch-item)
  ((macro-character :initform #\,)))

(defclass semicolon (terminated-macro)
  ((macro-character :initform #\;)
   (macro-terminating-character :initform #\newline)))

(defclass backquote (macro-character-mixin structure-branch-item)
  ((macro-character :initform #\`)))

;;;
;;; Dispatching Macro Characters
;;;

(defclass sharpsign-backslash (dispatching-macro-character-mixin structure-branch-item)
  ((dispatching-macro-character :initform #\#)
   (macro-character :initform #\\)
   (structure-content :type character)))

(defclass sharpsign-single-quote (dispatching-macro-character-mixin structure-branch-item)
  ((dispatching-macro-character :initform #\#)
   (macro-character :initform #\')))

(defclass sharpsign-left-parenthesis (dispatching-macro-character-mixin structure-branch-list)
  ((dispatching-macro-character :initform #\#)
   (macro-character :initform #\()))

(defclass sharpsign-asterisk (dispatching-macro-character-mixin structure-branch-list)
  ((dispatching-macro-character :initform #\#)
   (macro-character :initform #\*)))

(defclass sharpsign-colon (dispatching-macro-character-mixin structure-branch-item)
  ((dispatching-macro-character :initform #\#)
   (macro-character :initform #\:)))

(defclass sharpsign-dot (dispatching-macro-character-mixin structure-branch-item)
  ((dispatching-macro-character :initform #\#)
   (macro-character :initform #\.)))
